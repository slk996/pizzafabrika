const path = require('path');

module.exports = {
    entry: './src/index.jsx',
    output: {
        path: path.resolve(__dirname, 'html'),
        filename: 'bundle.js'
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },
    module: {
        rules: [{
            test: /\.js?x$/,
            loader: "babel-loader",
            exclude: /(node_modules|bower_components)/,
            options: {
                presets: ['env', 'es2015', 'react']
            }
        },
        {
            test: /\.json$/,
            loader: "json-loader"
        }
        ]
    },

    resolve: {
        extensions: ['.js', '.jsx','.json']
    }
}
