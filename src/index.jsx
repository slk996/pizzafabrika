import React from 'react';
import ReactDOM from 'react-dom';
import employeesData from './employees.json';
// const json = require('./employees');
import { App } from './main';

console.log(employeesData);

ReactDOM.render(
  <App employees={employeesData}/>,
  document.getElementById('root')
);