import React from 'react';
import { Header } from './header';
import { EmployeesList } from './employeesList.json';
import { Footer } from './footer'; 

export class App extends React.Component {
    constructor(props) {
        super();
        this.state = { defEmployees: props.employees };
        // console.log(props.employees);
    }

    render() {
        return <div>
            <Header />
            <EmployeesList defEmployees={this.state.defEmployees}/>
            <Footer />
        </div>
    }
}